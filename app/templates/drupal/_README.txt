
=================
FRONT-END TOOLING
(Non-Docker use)
=================

(Gulp and Node)

Requires:
Node.js (https://nodejs.org/en/)
Gulp (http://gulpjs.com/ ... Terminal command: "npm install gulp -g")

1.) Install this projects dependencies:
In Terminal, "cd" to this theme directory with "gulpfile.js" as root.
Run "npm install && gulp init".

2.) All gulp dependencies should be downloaded and you are good to go! Look in "gulpfile.js" for tasks. Here's a short list to get started:

// Watch CSS/JS/Images and compile on save
gulp watch

// Compile JS
gulp js

// Compile SASS to CSS
gulp css

// Compress images
gulp images

// Create modernizr.js file
gulp featureDetection

