const gulp =  require("gulp");
const sass =  require("gulp-sass");
const sourcemaps =  require('gulp-sourcemaps');
const stripCssComments =  require('gulp-strip-css-comments');
const uglify =  require('gulp-uglify');
const autoprefixer =  require('gulp-autoprefixer');
const changed =  require('gulp-changed');
const imagemin =  require('gulp-imagemin');
const modernizr =  require('gulp-modernizr');
const livereload =  require('gulp-livereload');
const { jsCompile } = require('rdg-drupal-js-lib');
const noop = require('gulp-noop');

/////////////////////////////
// Gulp Settings
/////////////////////////////

var production = false;

var gulpSettings = {
  sassOptions: {
    dev: {
      errLogToConsole: true,
      outputStyle: 'expanded',
      precision: 10
    },
    prod: {
      errLogToConsole: true,
      outputStyle: 'expanded',
      // outputStyle: 'compressed',
      precision: 10
    }
  },

  jsOptions: {
    mangle: false // Prevent Uglify from changing variable names
    // preserveComments: 'all'
  },

  css: {
    Src: 'src/sass/**/*.scss',
    Dest: 'build/css'
  },

  js: {
    Src: 'src/js/**/*.js',
    Dest: 'build/js'
  },

  images: {
    Src: 'src/images/**/*',
    Dest: 'build/images'
  },

  modernizrOptions: {
    "crawl" : false,
    "cache" : true,
    "options" : [
        "setClasses"
    ],
    "tests" : [
      'svg',
      'details', // Require by Drupal Core js
      'inputtypes', // Required by Drupal Core js
      'touchevents', // Required by Drupal Core js
      'addtest', // Required by Drupal Core js
      'prefixes', // Required by Drupal Core js
      'setclasses', // Required by Drupal Core js
      'teststyles', // Required by Drupal Core js
      'flexbox'
    ],
    "dest" : false
    // "excludeTests": [],
    // "customTests" : []
    // "devFile" : false,
    // "useBuffers": false,
    // "files" : {
    //     "src": [
    //         "*[^(g|G)runt(file)?].{js,css,scss}",
    //         "**[^node_modules]/**/*.{js,css,scss}",
    //         "!lib/**/*"
    //     ]
    // },
  }
};

/////////////////////////////
// Miscellaneous
/////////////////////////////

// Modernizr custom builds
const featureDetection = () =>
  gulp
  .src(gulpSettings.js.Dest + '/*.js')
  .pipe(modernizr(gulpSettings.modernizrOptions))
  .pipe(uglify())
  .pipe(gulp.dest('vendor/modernizr/'));

gulp.task('featureDetection', featureDetection);

/////////////////////////////
// CSS
/////////////////////////////

const css = () =>
  gulp
    .src(gulpSettings.css.Src)
    .pipe(sourcemaps.init())
    .pipe(sass(gulpSettings.sassOptions.prod).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer())
    .pipe(stripCssComments({ preserve: false }))
    .pipe(gulp.dest(gulpSettings.css.Dest))
    .pipe(production ? noop() : livereload());

gulp.task('css', css);

/////////////////////////////
// JS
/////////////////////////////

const js = () =>
  jsCompile(gulpSettings.js.Src, gulpSettings.js.Dest)
  .pipe(production ? noop() : livereload());

gulp.task('js', js);

/////////////////////////////
// IMAGES
/////////////////////////////

const images = () =>
  gulp
    .src(gulpSettings.images.Src)
    .pipe(changed(gulpSettings.images.Dest))
    .pipe(imagemin())
    .pipe(gulp.dest(gulpSettings.images.Dest))
    .pipe(production ? noop() : livereload());

gulp.task('images', images);

/////////////////////////////
// MISCELLANEOUS
/////////////////////////////

const watchCss = () => gulp.watch(gulpSettings.css.Src , css);
const watchJs = () => gulp.watch(gulpSettings.js.Src, js);
const watchImages = () => gulp.watch(gulpSettings.images.Src, images);

/////////////////////////////
// COMBINED TASKS
/////////////////////////////

gulp.task('init', gulp.parallel('css', 'js', 'images', 'featureDetection'));

gulp.task('build', gulp.parallel('css', 'js', 'images'));

// Default task to be run with `gulp`
gulp.task('default', gulp.parallel('build'));

gulp.task('watch',
  gulp.series(
    'build',
    async () => livereload.listen(),
    gulp.parallel(watchCss, watchJs, watchImages),
  )
);
