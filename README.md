# generator-drupal8theme [![Build Status](https://secure.travis-ci.org/jswitchback/generator-drupal8theme.png?branch=master)](https://travis-ci.org/jswitchback/generator-drupal8theme)

> [Yeoman](http://yeoman.io) generator

## Getting Started

### Install Yeoman

Yeoman is in the [npm](https://npmjs.org) package repository.

```bash
npm install -g yo
```

### Getting To Know Yeoman

Yeoman has a heart of gold. He's a person with feelings and opinions, but he's very easy to work with. If you think he's too opinionated, he can be easily convinced.

If you'd like to get to know Yeoman better check out the complete [Getting Started Guide](https://github.com/yeoman/yeoman/wiki/Getting-Started).

---

### Rapid Development Group

---

### Installing locally

Uninstall the old version first:

```bash
npm uninstall -g generator-drupal8theme
```

Run the following command to clone the repository, link to NPM and install dependencies:

```bash
git clone git@bitbucket.org:rapiddevelopmentgroup/generator-drupal8theme.git && cd generator-drupal8theme && npm link && npm install
```

Generate a new theme from templates in any directory. Omit *--skip-install* to allow node dependencies to be installed (useful for non-Docker projects).

```bash
yo drupal8theme --skip-install
```

Update local machine with latest code by doing a 'git pull' in the generator-drupal8theme directory


